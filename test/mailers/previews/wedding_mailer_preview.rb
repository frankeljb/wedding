# Preview all emails at http://localhost:3000/rails/mailers/wedding_mailer
class WeddingMailerPreview < ActionMailer::Preview
  def invitation_mail_preview
    WeddingMailer.invitation_email(Invitee.first)
  end
  def rsvp_email_preview
    WeddingMailer.rsvp_email(Invitee.first)
  end
end
