FactoryGirl.define do
  factory :invitee do
    name "Test User"
    number 4
    email "test@user.com"
    uuid SecureRandom.uuid
    id uuid
  end
end
