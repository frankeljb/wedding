require 'csv'

class Invitee < ActiveRecord::Base
  attr_accessor :is_attending_patch

  before_save :set_uuid

  def self.import(file)
    allowed_attributes = ["name", "addressee", "max_guests", "email"]
    CSV.foreach(file.path, headers: true, :col_sep => ";") do |row|
      invitee = find_by_uuid(row["id"]) || new
      invitee.attributes = row.to_hash.select { |k,v| allowed_attributes.include? k }
      invitee.save!
    end
  end

  def to_param
    uuid
  end

  private
    
    def set_uuid
      self.uuid = SecureRandom.uuid if !self.uuid
    end
end
