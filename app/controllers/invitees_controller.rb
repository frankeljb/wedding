class InviteesController < ApplicationController
  before_action :set_invitee, only: [:show, :edit, :update, :destroy]

  # GET /invitees
  # GET /invitees.json
  def index
    @invitees = Invitee.all
  end

  def index_datatable
    @invitees = nil #Invitee.all
    respond_to do |format|
      format.html
      format.json {
        render json: InviteesDatatable.new(view_context)
      }
    end
  end

  # GET /invitees/1
  # GET /invitees/1.json
  def show
    @invitee = Invitee.find_by_uuid(params[:id])
  end

  # GET /invitees/new
  def new
    @invitee = Invitee.new
  end

  # GET /invitees/1/edit
  def edit
  end

  # POST /invitees
  # POST /invitees.json
  def create
    @invitee = Invitee.new(invitee_params)

    respond_to do |format|
      if @invitee.save
        format.html { redirect_to @invitee, notice: 'Invitee was successfully created.' }
        format.json { render action: 'show', status: :created, location: @invitee }
      else
        format.html { render action: 'new' }
        format.json { render json: @invitee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invitees/1
  # PATCH/PUT /invitees/1.json
  def update
    respond_to do |format|
      is_attending_patch = params[:invitee][:is_attending_patch]
      if params[:invitee][:number].to_i > 0
        params[:invitee][:attending] = true
      else
        params[:invitee][:attending] = false
      end
      if !params[:invitee][:attending]
        params[:invitee][:number] = 0
      end
      if @invitee.update(invitee_params)
        if is_attending_patch
          WeddingMailer.rsvp_email(@invitee).deliver
          format.html { redirect_to rsvp_result_invitee_path(@invitee) }
        else
          format.html { redirect_to @invitee, notice: 'Invitee was successfully updated.' }
        end

        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @invitee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invitees/1
  # DELETE /invitees/1.json
  def destroy
    @invitee.destroy
    respond_to do |format|
      format.html { redirect_to invitees_url }
      format.json { head :no_content }
    end
  end

  def destroy_by_index
    if params[:index] == 'all'
      Invitee.destroy_all
    else
      Invitee.destroy(params[:index])
    end
    render :nothing => true
  end

  def attending
    @invitee = Invitee.find_by_uuid(params[:id])
  end

  def not_attending
    @invitee = Invitee.find_by_uuid(params[:id])
  end

  def rsvp_main
    @invitee = Invitee.find_by_uuid(params[:id])
  end

  def rsvp_result
    @invitee = Invitee.find_by_uuid(params[:id])
  end

  def mass_email
    if params[:index] == 'all' 
      @invitees = Invitee.all
    else
      @invitees = Invitee.find(params[:index])
    end
    @invitees.each do |invitee|
      # SendEmailJob.perform_later(@invitee)
      WeddingMailer.invitation_email(invitee).deliver
    end
    respond_to do |format|
      format.js
    end
  end

  def import
    Invitee.import(params[:file])
    render :nothing => true
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invitee
      #@invitee = Invitee.find(params[:id])
      @invitee = Invitee.find_by_uuid(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invitee_params
      params.require(:invitee).permit(:name, :number, :attending, :uuid, :email ,:is_attending_patch, :addressee)
    end
end
