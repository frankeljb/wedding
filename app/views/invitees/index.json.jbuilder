json.array!(@invitees) do |invitee|
  json.extract! invitee, :id, :name, :number, :attending, :uuid
  json.url invitee_url(invitee, format: :json)
end
