class InviteesDatatable
  delegate :params, :fa_icon, :check_box_tag, :link_to, :rsvp_main_invitee_url, :edit_invitee_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Invitee.count,
      iTotalDisplayRecords: invitees.total_entries,
      aaData: data
    }
  end

private

  def data
    puts "Invitees is #{invitees}"
    invitees.map do |invitee|
      [
        check_box_tag("#{:select_box}_#{invitee.id}", invitee.id, false, class: 'select'),
        link_to(invitee.name, invitee),
        invitee.addressee,
        invitee.email,
        invitee.number,
        invitee.max_guests,
        invitee.attending ? fa_icon('check') : fa_icon('close'),
        link_to(fa_icon('edit') + 'RSVP', rsvp_main_invitee_url(invitee), class: 'btn btn-primary btn-xs'),
        link_to('Edit', edit_invitee_path(invitee), class: 'btn btn-primary btn-xs'),
        link_to('Destroy', invitee, method: :delete, data: { confirm: 'Are you sure?' }, class: 'btn btn-primary btn-xs')
      ]
    end
  end

  def invitees
    @invitees ||= fetch_invitees
  end

  def fetch_invitees
    invitees = Invitee.order("#{sort_column} #{sort_direction}")
    invitees = invitees.page(page).per_page(per_page)
    if params[:search][:value] != ''
      invitees = invitees.where("name like :search or addressee like :search or email like :search", search: "%#{params[:search][:value]}%")
    end
    invitees
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

  def sort_column
    columns = %w[id name addressee email number max_guests attending]
    columns[params[:order]['0'][:column].to_i]
  end

  def sort_direction
    params[:order]['0'][:dir]
  end
end
