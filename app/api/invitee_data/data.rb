module InviteeData
  class Data < Grape::API
    error_formatter :json, Data::ErrorFormatter
    before do
      error!("401 Unauthorized", 401) unless authenticated
    end

    helpers do
      def authenticated
        user = User.find_by_email(params[:email])
        user && user.authenticate(params[:password])
      end
    end

    resource :invitee_data do
      desc "List all Invitee"
      get :all_invitees do
        Invitee.all
      end

      desc "Get invitee by id"
      params do
        requires :id, type: String
      end
      get do
        Invitee.find_by_uuid(params[:id])
      end

      desc "Create a new invitee"
      params do
        requires :name, type: String
        requires :number, type: Integer
        requires :max_guests, type: Integer
        requires :email, type: String
      end
      post do
        Invitee.create!({
          name: params[:name],
          number: params[:number],
          max_guests: params[:max_guests],
          email: params[:email]
        })
      end

      desc "Create multiple invitees"
      params do
        requires :invitees, type: Array do
          requires :name, type: String
          requires :number, type: Integer
          requires :max_guests, type: Integer
          requires :email, type: String
        end
      end
      post :multiple_invitees do
        params[:invitees].each do |invitee|
          Invitee.create!({
            name: invitee.name,
            number: invitee.number,
            max_guests: invitee.max_guests,
            email: invitee.email
          })
        end
      end



      desc "Delete an invitee"
      params do
        requires :id, type: String
      end
      delete do
        Invitee.find_by_uuid(params[:id]).destroy!
      end
    end
  end

end
