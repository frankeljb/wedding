class WeddingMailer < ActionMailer::Base
  default from: "mfriedland.jfrankel@gmail.com"

  def invitation_email invitee
    @invitee = invitee
    attachments.inline['invitation.png'] = File.read(Rails.root.join('app/assets/images/invitation.png'))
    attachments.inline['rsvp.png'] = File.read(Rails.root.join('app/assets/images/rsvp.png'))
    mail(to: @invitee.email, subject: "Invitation to Megan and Jonathan's Wedding")
  end

  def rsvp_email invitee
    @invitee = invitee
    mail(to: "mfriedland.jfrankel@gmail.com", subject: "RSVP From #{@invitee.name}")
  end
end
