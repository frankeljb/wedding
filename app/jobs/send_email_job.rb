class SendEmailJob < ActiveJob::Base
  queue_as :default

  def perform invitee
    #Delayed::Worker.logger.debug("Log Entry")
    WeddingMailer.invitation_email(invitee).deliver_later
  end
end
