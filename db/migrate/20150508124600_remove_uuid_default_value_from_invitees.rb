class RemoveUuidDefaultValueFromInvitees < ActiveRecord::Migration
  def up
    change_column :invitees, :uuid, :string, default: nil
  end

  def down
    change_column :invitees, :uuid, :string, default: SecureRandom.uuid
  end
end
