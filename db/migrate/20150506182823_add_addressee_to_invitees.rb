class AddAddresseeToInvitees < ActiveRecord::Migration
  def up
    add_column :invitees, :addressee, :string 
  end

  def down
    remove_column :invitees, :addressee
  end
end
