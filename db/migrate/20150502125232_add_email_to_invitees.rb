class AddEmailToInvitees < ActiveRecord::Migration
  def up
    add_column :invitees, :email, :string
  end

  def down
    remove_column  :invitees, :email
  end
end
