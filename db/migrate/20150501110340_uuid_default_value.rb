class UuidDefaultValue < ActiveRecord::Migration
  def up
    change_column :invitees, :uuid, :string, default: SecureRandom.uuid
  end
end
