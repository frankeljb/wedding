class AddMaxGuestsToInvitees < ActiveRecord::Migration
  def up
    add_column :invitees, :max_guests, :integer, :default => 0
  end

  def down
    remove_column :invitees, :max_guests
  end
end
