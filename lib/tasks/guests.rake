namespace :guests do
  desc "Format Guests to CSV"
  task :format => :environment do
    puts "Writting formatted guests to 'formatted_guests.csv'"
    File.open('formatted_guests.csv', 'w') { |f| f.puts("name;addressee;max_guests;email;number") } 

    File.open('lib/tasks/guests.csv', 'r').each_line do |f|
      line_split = f.split(";")
      match = f.match(/(.*)\s*\((\d+)\).*/)
      max_guests = match[2]
      if match[2].to_i == 0
        max_guests = match[1].split('and').size
      end
      invitee = { name: match[1], addressee: match[1], max_guests: max_guests, email: line_split[2], number: 0 }
      File.open('formatted_guests.csv', 'a') do |f| 
        f.puts(invitee.map { |k, v| v }.join(';'))
      end
    end
    puts "Finished formatting guests"
  end
end
